<?php

declare(strict_types=1);

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class SongController extends AbstractController
{
    #[Route('/api/songs/{id<\d+>}', name:'api_songs_get_one', methods: ['GET'])]
    public function getSong(LoggerInterface $logger, int $id): JsonResponse
    {
        // TODO Query the database
        $song = [
            'id' => $id,
            'name' => 'Waterfalls',
            'url' => 'https://symfonycasts.s3.amazonaws.com/sample.mp3',
        ];

        $logger->info('Returning API response of song {song}', ['song' => $id]);

        return $this->json($song);
    }
}
